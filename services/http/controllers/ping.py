from flask_restful import Resource
from flask.globals import request

from utils.resource import BaseResource
from utils.response import error_response, ok_response

class Ping(BaseResource):
    def get(self, id=None):
        param_json = request.args.to_dict() 
        return ok_response({
            "status": "All OK"
        })

    def put(self, id=None):
        request_data = request.get_json(force=True) 
        return error_response(401, [])

    def patch(self, id=None):
        request_data = request.get_json(force=True) 
        return error_response(401, [])

    def post(self):
        request_data = request.get_json(force=True) 
        return error_response(401, [])

    def delete(self, id=None):
        return error_response(401, [])

