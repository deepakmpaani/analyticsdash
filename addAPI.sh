#!/bin/bash

RESOURCE_NAME=$1 
RESOURCE_NAME_LOWER=${RESOURCE_NAME,,}


echo "from flask_restful import Resource
from flask.globals import request

from utils.resource import BaseResource
from utils.response import error_response, ok_response

class ResourceName(BaseResource):
    def get(self, id=None):
        param_json = request.args.to_dict() 
        return error_response(401, [])

    def put(self, id=None):
        request_data = request.get_json(force=True) 
        return error_response(401, [])

    def patch(self, id=None):
        request_data = request.get_json(force=True) 
        return error_response(401, [])

    def post(self):
        request_data = request.get_json(force=True) 
        return error_response(401, [])

    def delete(self, id=None):
        return error_response(401, [])
" | sed -r 's/ResourceName/'$RESOURCE_NAME'/' > services/http/controllers/$RESOURCE_NAME_LOWER.py 

SERVICE_APP_REPLACEMENT="api.add_resource("$RESOURCE_NAME", '"$RESOURCE_NAME_LOWER"/', '"$RESOURCE_NAME_LOWER"/<string:id>')"
IMPORT_STRING_REPLACEMENT="from services.http.controllers."$RESOURCE_NAME_LOWER" import "$RESOURCE_NAME


sed -i "s|#API END POINTS|#API END POINTS\n$SERVICE_APP_REPLACEMENT|g" service_app.py
sed -i "s|#SERVICE LAYER IMPORTS|#SERVICE LAYER IMPORTS\n$IMPORT_STRING_REPLACEMENT|g" service_app.py

