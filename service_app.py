from flask import Flask, request 
from flask.ext import restful

#SERVICE LAYER IMPORTS
from services.http.controllers.topcustomers import TopCustomers
from services.http.controllers.ping import Ping

app = Flask(__name__)
api = restful.Api(app, prefix='/analyticsdash/v1/')

#API END POINTS
api.add_resource(TopCustomers, 'topcustomers/', 'topcustomers/<string:id>')
api.add_resource(Ping, 'ping/', 'ping/<string:id>')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8803, debug=True)

