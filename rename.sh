#!/bin/bash

SERVICE_NAME=$1
echo $SERVICE_NAME
find ./ -exec sed -i "s/analyticsdash/$SERVICE_NAME/g" {} \;
