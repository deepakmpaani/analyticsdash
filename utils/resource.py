from functools import wraps
from flask import request, session, current_app as app
from flask.ext import restful

def is_2XX(code):
    return code / 100 == 2
        

def sanitize_response(response):
    http_status = 200
    data = None
    headers = {}

    if isinstance(response, tuple):
        (data, http_status, headers) = response
    else:
        data = response

    if is_2XX(http_status) and isinstance(data, list):
        data = dict(result=data)

    return (data, http_status, headers)


def format_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        data, http_status, headers = sanitize_response(func(*args, **kwargs))
        return (data, http_status, headers)
    return wrapper


def cors(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        data, status, headers = format_response(func(*args, **kwargs))

        cors_allow_headers = ', '.join(app.config.get('CORS_ALLOW_HEADERS', []))
        cors_allow_origins = ', '.join(app.config.get('CORS_ALLOW_ORIGINS', []))
        cors_allow_methods = ', '.join(app.config.get('CORS_ALLOW_METHODS', []))

        headers.update({
            'Access-Control-Allow-Headers': cors_allow_headers,
            'Access-Control-Allow-Origin': cors_allow_origins,
            'Access-Control-Allow-Methods': cors_allow_methods
        })

        return (data, status, headers)

    return wrapper


class BaseResource(restful.Resource):
    method_decorators = [format_response]
